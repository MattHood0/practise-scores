\version "2.18.2"

\header {
        title = "Sonata No. 29"
        opus = "Op. 106"
        composer = "L. van Beethoven"
}

global = \relative c {
        \tempo "Allegro" 2 = 138
        \key bes \major
        \partial 8 s8 |
        
}

upper = \relative c' {
        << {
                % Page 1
                \override Voice.Rest #'staff-position = #0
                r8 |
                r4 r8 <d' f bes d> q4-. <d f bes e>8-. <d f bes d>-. |
                q4-. <b d f bes>-. r2 |
                r4 r8 <f' bes d f> q4 <f bes d g>8 <f bes d f> |
                q4 <d f bes d> r\fermata bes8 c |
                d2 c4 c8 d |
                e2 d4 d8 e |
                f4. g8 f r4 f8 |
                e d f e c4 b8 c |
                d2 c4 c8 d |
                e2 d4 d8 e |
                f4. g8 f8 e4 f8 |
                e d4 e8 d8 c d e |
                g f g f f e f e |
                e d e d d c d e |
                g f r8 g~ g aes4 a8 |
                \ottava #1
                c' bes a g s2 | \ottava #0
                s1*14 |
        } \\ {
                % Page 1
                s8 |
                s1*4 |
                r4 g,, a aes |
                g a bes g | 
                f bes g a |
                <f bes> <g c> a r4 |
                r4 g a aes | 
                g a? bes g |
                f bes g a |
                f bes2 <f a>4 |
                <f bes> bes g a |
                f b2 <f a>4 |
                <f bes> <bes e> ees <c ees> |
                \ottava #1
                <bes' d> <bes ees> <b d f> <a c f a> |
                <bes, d f bes>2 \ottava #0 <bes d bes'> |
                <d f d'>4 <bes d bes'> <d f d'> <c e c'> |
                <ees g ees'>2 <c ees c'> |
                <ees g ees'>4 <cis e cis'> <ees ges ees'> <d f d'> |
                <f aes f'>2 <d f d'> |
                \ottava #1
                <f aes f'> <d fis d'> <f aes f'> <ees g ees'> |
                <g bes g'>2 <ees g bes ees'> |
                <g bes g'>4 <e bes' e> <ges bes ges'> <f bes f'> |
                <aes bes aes'> <fis bes fis'> <aes bes aes'> <g bes g'> |
                <bes ees g bes> <gis gis'> <bes bes'> <a a'> |
                <c c'> <bes bes'> <g g'> <f f'> | \ottava #0
                <ees ees'> <d d'> <c c'> <bes bes'> |
                <g g'> <f f'> <ees ees'> <d d'> |
                <c c'> <bes bes'> <g g'> <f f'> |
                
        } >>
        
}

lower = \relative c, {
        \clef "bass"
        
        % Page 1
        bes8 |
        
}
        

\score {
        \new PianoStaff <<
                \new Staff << \global  \upper >>
                \new Staff << \lower >>
        >>
}